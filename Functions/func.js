 //this section of code is for f1
var hi = function(){
    console.log("hello world as expression");
}
hi();

function hi2(){
    //console.log("Hello world as declaration");
    document.getElementById("output1").innerHTML = "Hello world as a declaration"
}

var button1 = document.createElement("Button");
button1.innerHTML = "Declaration with button";
var body  = document.getElementsByTagName("body")[0];
body.appendChild(button1);

button1.addEventListener ("click", function(){
    hi2()
});