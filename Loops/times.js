//Name: Johan Vermaak
//Date: 15/03/2018
//ID: 10015918

let num1 = 0;
let num2 = 0;
let answer = 0;
let input = "";

num2 = prompt("Which times table do you want to see?")

function for_loop(num1,num2){
    for(num1=1;num1 <= 12; num1++) {
        answer = num1*num2;
        console.log(`${num1} x ${num2} = ${answer}`);
    }
    
}
console.log("  >>>For Loop<<<")
for_loop(num1, num2);