//Name: Johan Vermaak
//Date: 15/03/2018
//ID: 10015918

let num1 = 0; //creating var for which code will use to make the x10 x100 and x1000 numbers
let num2 = 7;

function for_loop(num1){
    for(num1=3; num1<=7;num1++){ //telling the for loop to start from 3 and go to 7 in ascending
        console.log(`${num1}  ${num1*10}  ${num1*100} ${num1*1000}`); //everytime the for loop, loops it replaces num1 with the next number
    }
}
console.log(">>> for loop <<<")
for_loop(num1); // runs the for loop function


function while_loop(num1,num2){
    num1 = 3; 
    while(num1 <= num2){    //runs loop from 3 to 7 as num2=7
        console.log(`${num1}  ${num1*10}  ${num1*100} ${num1*1000}`);
        num1 ++;
    }
}
console.log("\n>>> While loop <<<");
while_loop(num1,num2);


function do_while(num1){
    num1 = 3
    do{
        console.log(`${num1}  ${num1*10}  ${num1*100} ${num1*1000}`);
        num1 ++
    }while(num1 <= num2)
}
console.log("\n>>> Do While loop <<<")
do_while(num1,num2)