let input = "";
let instruct = "What variety of fruit do you want the price of - Apples, Bananas\nKiwifruit, Oranges";
let nonfruit = "Sorry we did not recognize that fruit from our menu";


input = prompt(instruct);
input = input.toLowerCase();

switch(input) {
    case "apples":
        console.log(input + " are $1.25/kg today");
        break;
    case "bananas":
        console.log(input + " are $3.15/kg today");
        break;
    case "kiwifruit":
        console.log(input + " are $4.65/kg today");
        break;
    case "oranges":
        console.log(input + " are $2.75/kg today");
        break;
    default:
        console.log(nonfruit);
        break;
}