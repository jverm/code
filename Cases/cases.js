let petInput = "";
let msgCorrect = "You guessed my pet correctly";
let msgIncorrect = "You did not guess my pet correctly";
let Instruct = "Please input a type of pet to guess my favourite pet.\n" + "Guess from dogs, cats, birds and rabbits";

petInput = prompt(Instruct);
petInput = petInput.toLowerCase();

switch(petInput){
    case "dogs":
        console.log(msgCorrect);
        break;
    case "cats":
        console.log(msgIncorrect);
        break;
    case "birds":
        console.log(msgIncorrect);
        break;
    case "rabbits":
        console.log(msgIncorrect);
        break;
    default:
        console.log(Instruct);
        break;
}
