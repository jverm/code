let car = [];

let car1 = {year: "1998" , make: "Mitsubishi" , model: "Lancer"};
car.push(car1);

let string1 = "---Car 1---<br>";
for(let x in car1){
    string1 += car1[x] + " ";
}

document.getElementById("output").innerHTML = string1 + "<br><br>";

let car2 = {
    year: "2017",
    make: "BMW",
    model: "M3"
};
let string2 = "---Car 2---<br>";
for(let x in car2){
    string2 += car2[x] + " ";
}
document.getElementById("output2").innerHTML = string2 + "<br><br>";

let vehicle = function(){
    this.year;
    this.make;
    this.model;
} 
let car3 = new vehicle();
car3.year = "2008";
car3.make = "Audi";
car3.model = "R8";

let string3 = "---Car 3---<br>";
for(let x in car3){
    string3 += car3[x] + " ";
}
document.getElementById("output3").innerHTML = string3 + "<br><br>";