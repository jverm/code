let myFruit = 
[
    ["Banana", 2.99],
    ["Apple", 1.99],
    ["Mandarin", 4.75],
    ["Kiwifruit", 5.45],
    ["Orange", 3.65]
]
let numRow = 5;
let numCol = 2;

console.log(myFruit);   
console.log("----------------------");
for(let x in myFruit){
    console.log(myFruit[x])
}
console.log("----------------------");
for(let i=0; i < 5; i++){
    console.log(myFruit[i])
}
console.log("----------------------");
console.log(myFruit[2]);
console.log(myFruit[2][0]);
console.log(myFruit[2][1]);
console.log("----------------------");
console.log(myFruit[0][1]);
console.log(myFruit[1][1]);
console.log(myFruit[2][1]);
console.log(myFruit[3][1]);
console.log(myFruit[4][1]);
console.log("----------------------");
console.log("part c");
for(let i = 0; i < numRow; i++)
{
    for(let j = 0; j < numCol; j++)
    {
    console.log(`${myFruit[i][j]} \t\t i = ${i} \t j = ${j}`);
    }
}
console.log("----------------------");
for(let i = 0; i < numRow; i++)
{
    let rowStr = " ";
    for(let j = 0; j < numCol; j++)
    {
    rowStr = rowStr + myFruit[i][j] + " ";
    }
    console.log(rowStr)
}
   