let system = [
    ["Lenovo","Ultra Notebook","Thinkpad L470"],
    ["Dell","Blade Server","PowerEdge M830"],
    ["HP","All-in-One Desktop","EliteOne 800 G3"]
]
let row = 3;
let col = 3;

console.log(system);
console.log("----------------------");
for(let x in system){
    console.log(system[x]);
}
console.log("----------------------");
for(i=0;i<=2;i++){
    console.log(system[i]);
}
console.log("----------------------");
console.log(system[1]);
console.log("----------------------");
console.log(system[1][0]);
console.log("----------------------");
console.log(system[1][1]);
console.log("----------------------");
console.log(system[0][1]);
console.log(system[1][1]);
console.log(system[2][1]);
console.log("----------------------");
console.log("Part D");
for(i = 0; i < row; i++){
    for(j = 0; j < col; j++){
        console.log(`${system[i][j]} \t\t i = ${i} \t j=${j};`);
    }
}
console.log("----------------------");
for(i = 0; i < row; i++){
    let rowstr = " ";
    for(j = 0; j < col; j++){
        rowstr = rowstr + system[i][j] + " ";
    }
    console.log(rowstr);
}